import logoBiru from "../assets/img/logobiru.png";
import { FaFacebook } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { FaEnvelope } from "react-icons/fa";
import { FaTwitch } from "react-icons/fa";

const Footer = () => {
  return (
    <div className="footer">
      <div className="foot">
        <div className="adress">
          <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
          <p>binarcarrental@gmail.com</p>
          <p>081-233-334-808</p>
        </div>
        <div
          className="mb-3 fitur lh-lg d-flex flex-column"
          style={{ fontSize: "14px", fontWeight: "bold" }}
        >
          <a
            className="active text-decoration-none text-black"
            aria-current="page"
            href="#first-section"
          >
            Our Service
          </a>
          <a
            className="active text-decoration-none text-black"
            aria-current="page"
            href="#second-section"
          >
            Why Us
          </a>
          <a
            className="active text-decoration-none text-black"
            aria-current="page"
            href="#third-section"
          >
            Testimonial
          </a>
          <a
            className="active text-decoration-none text-black"
            aria-current="page"
            href="#fourth-section"
          >
            FAQ
          </a>
        </div>
        <div className="social-media">
          <p>Connect with us</p>
          <div className="connect" style={{ marginBottom: "60px" }}>
            <a
              className="fb text-decoration-none text-white"
              href="https://facebook.com"
            >
              <FaFacebook />
            </a>
            <a
              className="fb text-decoration-none text-white"
              href="https://instagram.com/diazzkurniawan"
            >
              <FaInstagram />
            </a>
            <a
              className="fb text-decoration-none text-white"
              href="https://twitter.com"
            >
              <FaTwitter />
            </a>
            <a
              className="fb text-decoration-none text-white"
              href="https://gmail.com"
            >
              <FaEnvelope />
            </a>
            <a
              className="fb text-decoration-none text-white"
              href="https://twitch.com"
            >
              <FaTwitch />
            </a>
          </div>
        </div>
        <div className="col-lg-3" style={{ fontSize: "14px" }}>
          <div className="mb-5">
            <p className="text-copy">Copyright Binar 2022</p>
            <a className="footer" href="#">
              <img className="text-copy" src={logoBiru} />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
