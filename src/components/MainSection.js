import car from "../assets/img/car.png";

const MainSection = () => {
  return (
    <div className="main" style={{ backgroundColor: "#f1f3ff" }}>
      <div className="container-fluid main-sect">
        <div className="box-desc">
          <h1 className="title-usaha">
            Sewa & Rental Mobil Terbaik di kawasan Mataram
          </h1>
          <p className="desc-usaha">
            Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
            untuk sewa mobil selama 24 jam.
          </p>
          <a
            className="btn btn-sm btn-success button-sewa"
            href="/cars"
            style={{ backgroundColor: "#5cb85f" }}
            type="button"
          >
            Mulai Sewa Mobil
          </a>
        </div>
        <div className="box-car">
          <img src={car} alt="" className="car-image" />
          <div className="background-blue"></div>
        </div>
      </div>
    </div>
  );
};

export default MainSection;
