import React from "react";
import Footer from "./components/Footer";
import Form from "./components/Form";
import NavBar from "./components/Navbar";
import car from "./assets/img/car.png";

export default function Sewa() {
  return (
    <div>
      <div>
        <NavBar />
      </div>
      <div>
        <div className="main" style={{ backgroundColor: "#f1f3ff" }}>
          <div className="container-fluid main-sect">
            <div className="box-desc">
              <h1 className="title-usaha">
                Sewa & Rental Mobil Terbaik di kawasan Mataram
              </h1>
              <p className="desc-usaha">
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
            </div>
            <div className="box-car">
              <img src={car} alt="" className="car-image" />
              <div className="background-blue"></div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <Form />
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}
