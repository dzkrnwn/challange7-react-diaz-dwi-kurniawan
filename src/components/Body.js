// Our Services
import girl from "../assets/img/girl.png";
import check from "../assets/img/check.png";

// Why Us
import { FaThumbsUp } from "react-icons/fa";
import { FaTag } from "react-icons/fa";
import { FaClock } from "react-icons/fa";
import { FaAward } from "react-icons/fa";

// Testimonial
import OwlCarousel from "react-owl-carousel";
import { BsStarFill } from "react-icons/bs";
import { Row, Col, Accordion, Card, Container } from "react-bootstrap";
import diaz from "../assets/img/diaz.jpg";
import cowo1 from "../assets/img/cowo1.png";
import cewe from "../assets/img/cewe.png";

const Body = () => {
  return (
    <>
      <div className="our" id="first-section">
        <div className="services">
          <div className="box-girl">
            <img src={girl} alt="" className="girl" />
          </div>
          <div className="box-kualitas">
            <h2 className="title-services">
              Best Car Rental for any kind of trip in Mataram!
            </h2>
            <p className="desc-services">
              Sewa mobil di Mataram bersama Binar Car Rental jaminan harga lebih
              murah dibandingkan yang lain, kondisi mobil baru, serta kualitas
              pelayanan terbaik untuk perjalanan wisata, bisnis, wedding,
              meeting, dll.
            </p>
            <div className="layanan">
              <div className="list">
                <div>
                  <img src={check} alt="" className="image-list" />
                </div>
                <div>
                  <img src={check} alt="" className="image-list" />
                </div>
                <div>
                  <img src={check} alt="" className="image-list" />
                </div>
                <div>
                  <img src={check} alt="" className="image-list" />
                </div>
                <div>
                  <img src={check} alt="" className="image-list" />
                </div>
              </div>
              <div className="keunggulan">
                <p>Sewa Mobil Dengan Supir di Mataram 12 Jam</p>
                <p>Sewa Mobil Lepas Kunci di Mataram 24 Jam</p>
                <p>Sewa Mobil Jangka Panjang Bulanan</p>
                <p>Gratis Antar - Jemput Mobil di Bandara</p>
                <p>Layanan Airport Transfer / Drop In Out</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* why us */}
      <div className="why" id="second-section">
        <div className="title-why">
          <h2>Why Us?</h2>
          <p className="question">Mengapa harus pilih Binar Car Rental?</p>
        </div>
        <div className="why-us">
          <div className="card-why card-section2 mb-3">
            <div className="jempol">
              <FaThumbsUp />
            </div>
            <h5 className="card-title">Mobil Lengkap</h5>
            <p className="card-text">
              Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
              terawat
            </p>
          </div>
          <div className="card-why card-section2 mb-3">
            <div className="harga-murah">
              <FaTag />
            </div>
            <h5 className="card-title">Harga Murah</h5>
            <p className="card-text">
              Harga murah dan bersaing, bisa bandingkan harga kami dengan rental
              mobil lain
            </p>
          </div>
          <div className="card-why card-section2 mb-3">
            <div className="clock">
              <FaClock />
            </div>
            <h5 className="card-title">Layanan 24 Jam</h5>
            <p className="card-text">
              Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
              tersedia di akhir minggu
            </p>
          </div>
          <div className="card-why card-section2 mb-3">
            <div className="award">
              <FaAward />
            </div>
            <h5 className="card-title">Sopir Profesional</h5>
            <p className="card-text">
              Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
              tepat waktu
            </p>
          </div>
        </div>
      </div>

      {/* Testimonial */}
      <section className="testimonial" id="testimonial">
        <div className="Testimonial-title text-center" id="Testimonial">
          <h2 className="fw-700">Testimonial</h2>
          <p>Berbagai review positif dari para pelanggan kami</p>
        </div>
        <OwlCarousel
          items={2}
          className="owl-theme"
          loop
          nav
          center
          margin={10}
        >
          <Row>
            <Col>
              <div className="item">
                <div className="testi-body">
                  <div className="card-left img-card">
                    <img src={cowo1} className="rounded-circle" />
                  </div>
                  <div className="card-right">
                    <div className="faq-icon">
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                    </div>
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur furete elit, sed
                      do eiusmod lorem ipsum dolor sit amet, consectetur
                      adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                      amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                    <p class="fw-bold">John Dee 32, Bromo</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="item">
                <div className="testi-body">
                  <div className="card-left img-card">
                    <img src={diaz} className="rounded-circle" />
                  </div>
                  <div className="card-right">
                    <div className="faq-icon">
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                    </div>
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur furete elit, sed
                      do eiusmod lorem ipsum dolor sit amet, consectetur
                      adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                      amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                    <p class="fw-bold">John Dee 32, Bromo</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="item">
                <div className="testi-body">
                  <div className="card-left img-card">
                    <img src={cewe} className="rounded-circle" />
                  </div>
                  <div className="card-right">
                    <div className="faq-icon">
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                    </div>
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur furete elit, sed
                      do eiusmod lorem ipsum dolor sit amet, consectetur
                      adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                      amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                    <p class="fw-bold">John Dee 32, Bromo</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </OwlCarousel>
      </section>

      {/* Banner */}
      <div className="getting">
        <div className="container get text-center">
          <h1>Sewa Mobil di Mataram Sekarang</h1>
          <div className="p-blue">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
          </div>
          <button
            className="btn btn-sm btn-success sewa"
            style={{ backgroundColor: "#5cb85f" }}
            type="button"
          >
            Mulai Sewa Mobil
          </button>
        </div>
      </div>

      {/* FAQ */}
      <section className="faq" id="faq">
        <Container>
          <Row>
            <Col md={5} className="faq-title">
              <h1 className="fw-bold faq-title1">Frequently Asked Question</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
            </Col>
            <Col md={7}>
              <Accordion className="faq-content">
                <Card className="faq-card">
                  <Accordion.Item eventKey="0">
                    <Accordion.Header>
                      Apa saja syarat yang dibutuhkan
                    </Accordion.Header>
                    <Accordion.Body className="faq-p">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Distinctio tempora ad quibusdam necessitatibus, quod
                      blanditiis aliquam dolorum illo vel omnis quam? Nam
                      exercitationem vitae odit sapiente, ab impedit sint
                      praesentium?
                    </Accordion.Body>
                  </Accordion.Item>
                </Card>

                <Card className="faq-card">
                  <Accordion.Item eventKey="1">
                    <Accordion.Header>
                      Berapa hari minimal sewa mobil lepas kunci?
                    </Accordion.Header>
                    <Accordion.Body className="faq-p">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Distinctio tempora ad quibusdam necessitatibus, quod
                      blanditiis aliquam dolorum illo vel omnis quam? Nam
                      exercitationem vitae odit sapiente, ab impedit sint
                      praesentium?
                    </Accordion.Body>
                  </Accordion.Item>
                </Card>

                <Card className="faq-card">
                  <Accordion.Item eventKey="2">
                    <Accordion.Header>
                      Berapa hari sebelumnya sabaiknya booking sewa mobil?
                    </Accordion.Header>
                    <Accordion.Body className="faq-p">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Distinctio tempora ad quibusdam necessitatibus, quod
                      blanditiis aliquam dolorum illo vel omnis quam? Nam
                      exercitationem vitae odit sapiente, ab impedit sint
                      praesentium?
                    </Accordion.Body>
                  </Accordion.Item>
                </Card>

                <Card className="faq-card">
                  <Accordion.Item eventKey="3">
                    <Accordion.Header>
                      Apakah Ada biaya antar-jemput?
                    </Accordion.Header>
                    <Accordion.Body className="faq-p">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Distinctio tempora ad quibusdam necessitatibus, quod
                      blanditiis aliquam dolorum illo vel omnis quam? Nam
                      exercitationem vitae odit sapiente, ab impedit sint
                      praesentium?
                    </Accordion.Body>
                  </Accordion.Item>
                </Card>

                <Card className="faq-card">
                  <Accordion.Item eventKey="4">
                    <Accordion.Header>
                      Bagaimana jika terjadi kecelakaan
                    </Accordion.Header>
                    <Accordion.Body className="faq-p">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Distinctio tempora ad quibusdam necessitatibus, quod
                      blanditiis aliquam dolorum illo vel omnis quam? Nam
                      exercitationem vitae odit sapiente, ab impedit sint
                      praesentium?
                    </Accordion.Body>
                  </Accordion.Item>
                </Card>
              </Accordion>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};

export default Body;
