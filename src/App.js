import "./App.css";
import MainSection from "./components/MainSection";
import NavBar from "./components/Navbar";
import Footer from "./components/Footer";
import Body from "./components/Body";

function App() {
  return (
    <div>
      <div>
        <NavBar />
      </div>

      <div>
        <MainSection />
      </div>

      <div>
        <Body />
      </div>

      <div>
        <Footer />
      </div>
    </div>
  );
}

export default App;
