import { Button, Nav } from "react-bootstrap";
import logoBiru from "../assets/img/logobiru.png";

const NavBar = () => {
  return (
    <Nav
      className="navbar navbar-expand-lg navbar-light"
      style={{ backgroundColor: "#f1f3ff" }}
    >
      <div className="container-fluid navbar-rental">
        <a className="navbar-brand" href="#">
          <img src={logoBiru} alt="" width="100" height="34" />
        </a>
        <Button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasRight"
          aria-controls="offcanvasRight"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </Button>
        <div
          className="offcanvas offcanvas-end off-navbar"
          id="offcanvasRight"
          aria-labelledby="offcanvasRightLabel"
        >
          <div className="offcanvas-header text-black-50">
            <h5 id="offcanvasRightLabel">
              <strong>BCR</strong>
            </h5>
            <Button
              type="button"
              className="btn-close text-reset"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></Button>
          </div>
          <div className="offcanvas-body">
            <div className="navbar-nav ms-auto navigation">
              <a
                className="nav-link active"
                aria-current="page"
                href="#first-section"
              >
                Our Services
              </a>
              <a className="nav-link" href="#second-section">
                Why Us
              </a>
              <a className="nav-link" href="#third-section">
                Testimonial
              </a>
              <a className="nav-link" href="#fourth-section">
                FAQ
              </a>
              <Button
                className="btn btn-md text-white register"
                type="button"
                style={{ backgroundColor: "#5cb85f" }}
              >
                Register
              </Button>
            </div>
          </div>
        </div>
      </div>
    </Nav>
  );
};

export default NavBar;
